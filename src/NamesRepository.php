<?php

namespace PTibbetts;

class NamesRepository
{

    /**
     * @return array
     */
    public function given()
    {
        return $this->readFilesIn('names/given');
    }

    /**
     * @return array
     */
    public function surnames()
    {
        return $this->readFilesIn('names/surnames');
    }

    /**
     * @return array
     */
    private function readFilesIn($directory)
    {
        $contents = [];

        $parent = dirname(dirname(__FILE__));
        $path   = $parent.'/'.$directory;
        $files  = scandir($path);

        foreach ($files as $file)
        {
            $lines = file($path.'/'.$file, FILE_IGNORE_NEW_LINES);

            foreach ($lines as $line)
            {
                array_push($contents, $line);
            }
        }

        return $contents;
    }

}
